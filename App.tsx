import Routes from './src/routes/Routes';
import {Provider} from 'react-redux';
import store from './src/redux/stores/store';
const App = () => {
  return (
    <Provider store={store}>
      <Routes />
    </Provider>
  );
};

export default App;
