import config from '../config/api';

const socket = new WebSocket(config.apiUrl);

export default socket;
