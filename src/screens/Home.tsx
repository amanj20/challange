import {NativeStackScreenProps} from '@react-navigation/native-stack';
import React, {useEffect} from 'react';
type Props = NativeStackScreenProps<any>;

import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../redux/stores/store';
import {subscribe} from '../redux/actions/actions';
import {SafeAreaView, ScrollView, View, Text} from 'react-native';

export default function Home({navigation}: Props) {
  const dispatch = useDispatch();
  const items = useSelector((state: RootState) => state.items);
  const keys = Object.entries(items);
  const result = keys.map(([key, value]) => {
    return value;
  });

  useEffect(() => {
    dispatch(
      subscribe([
        'ethusdt@trade',
        'btcusdt@trade',
        'ethbtc@trade',
        'ltcusdt@trade',
      ]),
    );
  }, []);

  return (
    <SafeAreaView>
      <ScrollView>
        {result?.map((item: any, index) => {
          return (
            <View
              key={index}
              style={{
                flexDirection: 'row',
                marginVertical: 16,
                marginHorizontal: 16,
                justifyContent: 'space-around',
              }}>
              <Text>{item?.data?.s}</Text>
              <Text
                style={{
                  color: '#fff',
                  backgroundColor: item?.color ? item?.color : '#fff',
                  paddingHorizontal: 16,
                  paddingVertical: 8,
                  borderRadius: 30,
                }}>
                {parseFloat(item?.data?.p).toFixed(2)}
              </Text>
            </View>
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
}
