import socket from '../../utils/socket';

export const STORE_DATA = 'STORE_DATA';
export const GET_ASYNC_DATA = 'GET_ASYNC_DATA';
export const SUBSCRIBE = 'SUBSCRIBE';
export function subscribe(params: string[]) {
  socket.onopen = () => {
    socket.send(
      JSON.stringify({
        method: 'SUBSCRIBE',
        params,
        id: 1,
      }),
    );
  };
  return {
    type: SUBSCRIBE,
  };
}

export function storeData(data: any) {
  return {
    type: STORE_DATA,
    payload: data,
  };
}
