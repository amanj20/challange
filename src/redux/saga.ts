import {eventChannel, END} from 'redux-saga';
import {put, take, call, takeEvery} from 'redux-saga/effects';
import socket from '../utils/socket';
import {STORE_DATA, SUBSCRIBE} from './actions/actions';

const getData = (socket: WebSocket) => {
  socket.onclose = () => {
    console.log('Disconnected');
  };

  return eventChannel((emitter: any) => {
    socket.onmessage = message => {
      emitter(message?.data);
    };
    const unsubscribe = () => {
      socket.close();
    };

    return unsubscribe;
  });
};

function* runAction(): Generator {
  const channel: any = yield call(getData, socket);
  while (true) {
    try {
      const data = yield take(channel);
      yield put({type: STORE_DATA, payload: data});
    } catch (err) {
      console.log('saga error:', err);
    }
  }
}

function* getAsyncData() {
  yield takeEvery(SUBSCRIBE, runAction);
}

export default getAsyncData;
