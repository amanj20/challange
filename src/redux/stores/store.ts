import {applyMiddleware, configureStore} from '@reduxjs/toolkit';
import {runSaga} from 'redux-saga';
import itemReducer from '../reducers/itemReducer';
import createSagaMiddleware from 'redux-saga';
import getAsyncData from '../saga';

const sagaMiddleware = createSagaMiddleware();
const store = configureStore({
  reducer: {
    items: itemReducer,
  },
  middleware: [sagaMiddleware],
});
sagaMiddleware.run(getAsyncData);
export default store;
export type RootState = ReturnType<typeof store.getState>;
