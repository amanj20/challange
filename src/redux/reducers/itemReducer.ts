import socket from '../../utils/socket';
import {STORE_DATA} from '../actions/actions';
let items: any = {};
var oldData: any;
export default function itemReducer(state = items, action: any) {
  //console.log(action.payload);
  switch (action.type) {
    case STORE_DATA:
      const res = JSON.parse(action?.payload);

      const newData = {
        ...state,
        [res?.data?.s]: {
          ...JSON.parse(action.payload),
        },
      };
      const dataWithColorProp = {
        ...state,
        [res?.data?.s]: {
          ...JSON.parse(action.payload),
          color: oldData
            ? oldData?.[res?.data?.s]?.data?.p >
              newData?.[res?.data?.s]?.data?.p
              ? '#DB1F2F'
              : '#3AC04A'
            : '#000',
        },
      };

      oldData = newData;

      delete dataWithColorProp['undefined'];
      return dataWithColorProp;
    default:
      return state;
  }
}
